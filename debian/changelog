flickcurl (1.26-8) UNRELEASED; urgency=medium

  * Orphan package

 -- Kumar Appaiah <akumar@debian.org>  Mon, 21 Aug 2023 18:18:18 +0530

flickcurl (1.26-7) unstable; urgency=medium

  * Reuploaded for source-only upload.

 -- Kumar Appaiah <akumar@debian.org>  Wed, 15 Sep 2021 17:09:23 +0530

flickcurl (1.26-6) unstable; urgency=medium

  * Depend on gtk-doc-tools to work around FTBFS with autotools 2.7+
    (Closes: #978809)
  * Standards version is now 4.5.1 (no changes)
  * libflickcurl0-dbg is now priority optional
  * debheler compat is now 12 (depend on debhelper 12)
  * Remove dh-autoreconf dependency
  
 -- Kumar Appaiah <akumar@debian.org>  Tue, 24 Aug 2021 20:53:32 +0530

flickcurl (1.26-5) unstable; urgency=medium

  * Add zlib1g-dev and liblzma-dev to Build-Depends to fix libxml2
    detection (Closes: #951930)
  * Add new VCS URIs after moving to salsa.debian.org
  * Priority of debug package is now optional

 -- Kumar Appaiah <akumar@debian.org>  Sun, 23 Feb 2020 19:17:23 +0530

flickcurl (1.26-4) unstable; urgency=medium

  * Apply patch from upstream to fix oauth token fetching
  * Apply patch from upstream to prevent double free corruption
    during authentication (Closes: #875800)
  * Update Standards Version to 4.0.0 (no changes)

 -- Kumar Appaiah <akumar@debian.org>  Fri, 15 Sep 2017 20:06:21 +0530

flickcurl (1.26-3) unstable; urgency=medium

  * Remove broken devhelp link in flickcurl-doc (Closes: #858513)

 -- Kumar Appaiah <akumar@debian.org>  Thu, 30 Mar 2017 07:25:12 +0530

flickcurl (1.26-2) unstable; urgency=medium

  * libflickcurl-dev: depend on libraptor2-dev, not obsolete
    libraptor1-dev (Closes: #854033). Simon McVittie's patch.

 -- Kumar Appaiah <akumar@debian.org>  Fri, 03 Feb 2017 16:42:40 +0530

flickcurl (1.26-1) unstable; urgency=medium

  * New upstream release (Closes: #850725)
  * Depend on libraptor2-dev (Closes: #850727)
  * Standards version is now 3.9.8 (no changes needed)
  * Remove HTTPS URL fix patches

 -- Kumar Appaiah <akumar@debian.org>  Tue, 10 Jan 2017 19:38:00 +0530

flickcurl (1.25-3) unstable; urgency=medium

  * Move to HTTPS URLs for authentication (Closes: #753074)

 -- Kumar Appaiah <akumar@debian.org>  Fri, 11 Jul 2014 13:12:51 +0530

flickcurl (1.25-2) unstable; urgency=low

  * Use Logan Rosen's patch to run autoreconf to support ppc4el (from
    Ubuntu). (Closes: #735948)

 -- Kumar Appaiah <akumar@debian.org>  Sat, 18 Jan 2014 17:48:05 -0500

flickcurl (1.25-1) unstable; urgency=low

  * New upstream release
  * Standards version is now 3.9.5 (no changes needed)
  * Multiarch conversion
    + Switch to dh based rules file
    + Add ${misc:Pre-Depends} and Multi-Arch: same for libflickcurl0
    + Alter install files to accommodate the multiarch path

 -- Kumar Appaiah <akumar@debian.org>  Sun, 12 Jan 2014 08:20:42 -0500

flickcurl (1.24-1) unstable; urgency=low

  * New upstream release

 -- Kumar Appaiah <akumar@debian.org>  Mon, 20 May 2013 21:15:09 -0400

flickcurl (1.23-1) experimental; urgency=low

  * New upstream release
    + OAuth authentication included (Closes: #700050)
  * debian/control:
    + Remove duplicate "libs" section for libflickcurl0
    + Standards Version 3.9.4 (no changes needed)
  * debian/rules: Add hardening flags
  * debian/patches/01-fix-tagread.diff
    +  Backport patch to fix tag reading

 -- Kumar Appaiah <akumar@debian.org>  Thu, 07 Feb 2013 19:35:21 -0600

flickcurl (1.22-1) unstable; urgency=low

  * New upstream release (Closes: #635892, #637196)
  * No need to use quilt (patch removed)

 -- Kumar Appaiah <akumar@debian.org>  Tue, 07 Feb 2012 22:46:03 -0600

flickcurl (1.21-5) unstable; urgency=low

  * Fix incorrect quilt patching order (Closes: #637795)

 -- Kumar Appaiah <akumar@debian.org>  Sun, 14 Aug 2011 10:20:49 -0500

flickcurl (1.21-4) unstable; urgency=low

  * Fix include of deprecated (removed) curl/types.h (Closes: #637700)

 -- Kumar Appaiah <akumar@debian.org>  Sat, 13 Aug 2011 17:14:59 -0500

flickcurl (1.21-3) unstable; urgency=low

  * Actually remove the la file during package build. (Closes: #635378).
  * Standards version is now 3.9.2 (no changes needed).
  * Update build rule to comply with lintian recommendations (provide
    build-arch and build-indep)

 -- Kumar Appaiah <akumar@debian.org>  Mon, 25 Jul 2011 20:32:57 -0500

flickcurl (1.21-2) unstable; urgency=low

  * Aboid installing libflickcurl.la (Closes: #621200)

 -- Kumar Appaiah <akumar@debian.org>  Thu, 07 Apr 2011 20:53:38 -0500

flickcurl (1.21-1) unstable; urgency=low

  * New upstream release

 -- Kumar Appaiah <akumar@debian.org>  Thu, 31 Mar 2011 23:54:10 -0500

flickcurl (1.19-2) unstable; urgency=low

  * libflickcurl-dev should depend on libraptor1-dev. Thanks to Hubert
    Figuiere for pointing this out. (Closes: #602156)
  * Standards Version is now 3.9.1 (No changes needed).

 -- Kumar Appaiah <akumar@debian.org>  Mon, 01 Nov 2010 23:01:55 -0500

flickcurl (1.19-1) unstable; urgency=low

  * New upstream release
  * Standards Version is now 3.9.0 (no changes needed)

 -- Kumar Appaiah <akumar@debian.org>  Tue, 27 Jul 2010 22:32:41 -0500

flickcurl (1.18-1) unstable; urgency=low

  * New upstream release.

 -- Kumar Appaiah <akumar@debian.org>  Mon, 26 Apr 2010 22:16:17 -0500

flickcurl (1.17-1) unstable; urgency=low

  * New upstream release.
  * Move dwww to "Suggests". (LP: #524799)
  * Standards Version is now 3.8.4 (no changes needed).

 -- Kumar Appaiah <akumar@debian.org>  Sat, 20 Feb 2010 22:21:16 -0600

flickcurl (1.16-1) unstable; urgency=low

  * New Upstream release.
  * debian/control: Add ${misc:Depends} for binary packages.

 -- Kumar Appaiah <akumar@debian.org>  Thu, 21 Jan 2010 07:30:40 -0600

flickcurl (1.14-2) unstable; urgency=low

  * Fix missing include pointed out by Vincent Fourmond (Closes: #559372)
    + libflickcurl-dev depends on libxml2-dev
    + flickcurl-config now prints the correct libraries to be included.
  * Switch to 3.0 (quilt) packaging.
  
 -- Kumar Appaiah <akumar@debian.org>  Thu, 03 Dec 2009 19:36:36 -0600

flickcurl (1.14-1) unstable; urgency=low

  * New upstream release
  * debian/libflickcurl-dev.examples: Add
    search-photos.c.
  * Standards Version is now 3.8.3 (No changes needed).

 -- Kumar Appaiah <akumar@debian.org>  Sun, 20 Sep 2009 17:25:42 -0500

flickcurl (1.13-1) unstable; urgency=low

  * New upstream release

 -- Kumar Appaiah <akumar@debian.org>  Mon, 03 Aug 2009 13:30:56 -0500

flickcurl (1.12-1) unstable; urgency=low

  * New upstream release.
  * quilt no longer needed, as patch merged upstream.

 -- Kumar Appaiah <akumar@debian.org>  Wed, 15 Jul 2009 19:00:26 -0500

flickcurl (1.11-1) unstable; urgency=low

  * New Upstream Version
  * Use quilt for patches.
  * Standards version is now 3.8.2 (no changes).
  * debian/patches: Fix null shape detection,
    to avoid Segfault.
  * debian/control: libflickcurl0-dbg pulls in
    flickcurl-utils for debugging. (Closes: #464190).

 -- Kumar Appaiah <akumar@debian.org>  Fri, 03 Jul 2009 08:03:32 -0400

flickcurl (1.10-1) unstable; urgency=low

  * New upstream release.
    + Upload URL fixed. (Closes: #526189)
  * debian/control:
    + Move libflickcurl0-dbg to debug section.

 -- Kumar Appaiah <akumar@debian.org>  Fri, 01 May 2009 19:31:36 -0500

flickcurl (1.9-1) unstable; urgency=low

  * New upstream release.
  * Standards version is now 3.8.1 (no changes needed).
  * Move libflickcurl0-dbg to debug section.

 -- Kumar Appaiah <akumar@debian.org>  Sun, 26 Apr 2009 20:45:13 -0500

flickcurl (1.8-1) unstable; urgency=low

  * New upstream release.
  * Upload to unstable.
  * debian/copyright: Update dates.

 -- Kumar Appaiah <akumar@debian.org>  Sun, 15 Feb 2009 10:35:05 -0600

flickcurl (1.7-1) experimental; urgency=low

  * New upstream release.
  * Fix location of devhelp symlinks.
  * Use the "©" symbol in debian.copyright.
  
 -- Kumar Appaiah <akumar@debian.org>  Sat, 17 Jan 2009 18:45:45 -0600

flickcurl (1.6-1) experimental; urgency=low

  * New upstream release.
  * Include HTML documentation in new flickcurl-doc package.
    - Ensure that symlinks are created for devhelp files.

 -- Kumar Appaiah <akumar@debian.org>  Mon, 15 Sep 2008 17:45:25 -0500

flickcurl (1.5-1) experimental; urgency=low

  * New upstream release.

 -- Kumar Appaiah <akumar@debian.org>  Thu, 04 Sep 2008 13:33:30 -0500

flickcurl (1.4-1) experimental; urgency=low

  * New upstream release.
    + Patches removed; merged upstream.
  * debian/control:
    + Fix typo in description.
    + Standards version now 3.8.0 (No changes needed).
    + Update my e-mail address.
    + quilt not needed.
  * debian/rules:
    + Remove quilt includes.
    + Alter example file name to print-photo-info.c.
  * debian/copyright: Point to common Apache license,
      rather than including it in full.
  
 -- Kumar Appaiah <akumar@debian.org>  Sat, 16 Aug 2008 20:47:30 -0500

flickcurl (1.2-1) unstable; urgency=low

  * New upstream release.
    + Document all supported API calls in man page.
      (Closes: #461852, #461846)
    + Don't specify USER-NSID as USER-ID.
      (Closes: #461858)
  * debian/control:
    + Fix incorrect Vcs-* fields. (Closes: #461843)
  * debian/patches:
    + user-nsid_fix.diff: Refer to USER-NSID instead of
      USER-ID in flickcurl.
    + fix_nullarg.diff: Don't segfault if insufficient
      parameters are provided. (Closes: #461854)
  * debian/rules:
    + Detect if cross-building occurs.

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Tue, 29 Jan 2008 16:08:56 +0530

flickcurl (1.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Add Dave Beckett (Upstream author) to Uploaders.
    + Update description to reflect 100% API support.
    + Depend on libraptor1-dev instead of the virtual
      package libraptor-dev and libxml-dev.
    + Depend on debhelper 6 and above.
  * debian/compat:
    + Use 6 for debhelper version increase.
  
 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Wed, 16 Jan 2008 09:55:28 +0530

flickcurl (0.13-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Add Homepage field, remove Homepage from description.
    + Bump standards version to 3.7.3 (No changes needed).
  * debian/copyright:
    + Update to add Vanilla Shu.
  * debian/*manpages:
    + Use upstream supplied man pages.
  
 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Sat, 29 Dec 2007 17:07:59 +0530

flickcurl (0.12-2) unstable; urgency=low

  * Fix watch file to use upstream web page directly.

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Sun, 23 Sep 2007 20:47:29 +0530

flickcurl (0.12-1) unstable; urgency=low

  * Initial release (Closes: #436065)

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Sun, 05 Aug 2007 13:21:54 +0530
