Source: flickcurl
Priority: optional
Section: libs
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 12), libraptor2-dev, dpkg-dev (>= 1.16.1~), zlib1g-dev, liblzma-dev, gtk-doc-tools
Standards-Version: 4.5.1
Homepage: http://librdf.org/flickcurl/
Vcs-Git: https://salsa.debian.org/akumar/flickcurl.git
Vcs-Browser: https://salsa.debian.org/akumar/flickcurl

Package: libflickcurl-dev
Section: libdevel
Architecture: any
Depends: libflickcurl0 (= ${binary:Version}), libxml2-dev, libraptor2-dev, ${misc:Depends}
Description: C library for accessing the Flickr API - development files
 Flickcurl is a C library for the Flickr API, handling creating the
 requests, signing, token management, calling the API, marshalling
 request parameters and decoding responses. The library now supports
 100% of the 2008-01-11 version of the API, including the functions
 for photo uploading, browsing, searching, adding and editing
 comments, groups, notes, photosets, categories, activity, blogs,
 favorites, places, tags and photo metadata. It also includes a
 program flickrdf to turn photo metadata, tags and machine tags into
 RDF descriptions of photos and tags.
 .
 This package contains the development libraries and headers for
 flickcurl.

Package: libflickcurl0
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: C library for accessing the Flickr API
 Flickcurl is a C library for the Flickr API, handling creating the
 requests, signing, token management, calling the API, marshalling
 request parameters and decoding responses. The library now supports
 100% of the 2008-01-11 version of the API, including the functions
 for photo uploading, browsing, searching, adding and editing
 comments, groups, notes, photosets, categories, activity, blogs,
 favorites, places, tags and photo metadata. It also includes a
 program flickrdf to turn photo metadata, tags and machine tags into
 RDF descriptions of photos and tags.

Package: libflickcurl0-dbg
Priority: optional
Section: debug
Architecture: any
Depends: libflickcurl0 (= ${binary:Version}), flickcurl-utils (= ${binary:Version}), ${misc:Depends}
Description: C library for accessing the Flickr API - debugging symbols
 Flickcurl is a C library for the Flickr API, handling creating the
 requests, signing, token management, calling the API, marshalling
 request parameters and decoding responses. The library now supports
 100% of the 2008-01-11 version of the API, including the functions
 for photo uploading, browsing, searching, adding and editing
 comments, groups, notes, photosets, categories, activity, blogs,
 favorites, places, tags and photo metadata. It also includes a
 program flickrdf to turn photo metadata, tags and machine tags into
 RDF descriptions of photos and tags.
 .
 This package contains the debugging symbols for debugging
 applications which use libflickurl0.

Package: flickcurl-utils
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: utilities to call the Flickr API from command line
 Flickcurl is a C library for the Flickr API, handling creating the
 requests, signing, token management, calling the API, marshalling
 request parameters and decoding responses. The library now supports
 100% of the 2008-01-11 version of the API, including the functions
 for photo uploading, browsing, searching, adding and editing
 comments, groups, notes, photosets, categories, activity, blogs,
 favorites, places, tags and photo metadata. It also includes a
 program flickrdf to turn photo metadata, tags and machine tags into
 RDF descriptions of photos and tags.
 .
 Support for the Flickr API in these programs is through the
 libflickcurl library.

Package: flickcurl-doc
Section: doc
Architecture: all
Recommends: www-browser
Suggests: dwww
Depends: ${misc:Depends}
Description: utilities to call the Flickr API from command line - documentation
 Flickcurl is a C library for the Flickr API, handling creating the
 requests, signing, token management, calling the API, marshalling
 request parameters and decoding responses. The library now supports
 100% of the 2008-01-11 version of the API, including the functions
 for photo uploading, browsing, searching, adding and editing
 comments, groups, notes, photosets, categories, activity, blogs,
 favorites, places, tags and photo metadata. It also includes a
 program flickrdf to turn photo metadata, tags and machine tags into
 RDF descriptions of photos and tags.
 .
 Support for the Flickr API in these programs is through the
 libflickcurl library.
 .
 This package contains the HTML documentation for flickcurl and the
 related library.
